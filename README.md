<div align="center">

 # Gerstlix project :: Translations
 ***
Данный репозиторий используется для мульти-язяковой системы с проекта **Gerstlix Network (mc.gerstlixnetwork.ru)**
</div>

---
## Обратная связь
Если у Вас есть какие-то предложения или желание в помощи по переводу, 
то можете обратиться к нам через указанные контакты:

* **[Discord](https://gerstlixnetwork.ru/discord)**
* **[ВКонтакте](https://vk.me/gerstlixnetwork)**
